var NSLib = {
	// TODO: add more options like customizing the text and icons
    // Custom files relies on the Bootstrap icons library
    SetupCustomFiles: function() {
		// Prevent the browser from loading a file when a user drags one into the window
		window.addEventListener("drop", function(e) {
			e.preventDefault();
		});
		window.addEventListener("dragover", function(e) {
			e.preventDefault();
		});

		// Find all elements with class .custom-file-input
		document.querySelectorAll(".custom-file-input").forEach(inputBox => {
			// Setup the inner elements if they do not already exist
			if (inputBox.querySelectorAll(".custom-file-input-wrapper").length == 0) {
				inputBox.innerHTML += `
				<div class="custom-file-input-wrapper">
					<div class="custom-file-beforeupload">
						<i class="bi-upload"></i>
						<br>
						<span>Choose a file or drag it here</span>
					</div>

					<div class="custom-file-afterupload">
						<i class="bi-check-lg"></i>
						<br>
						<span class="custom-file-label"></span>
					</div>
				</div>`;
			}

			// Find the input box inside the element
			var fileInput = inputBox.querySelector("input[type=file]");

			// Handle the file upload event
			fileInput.addEventListener("change", function() {
				if (fileInput.files.length > 0) {
					// Hide the "before upload" div
					inputBox.getElementsByClassName("custom-file-beforeupload")[0].style.display = "none";

					// Create file reader
					var fileReader = new FileReader();
					fileReader.onload = function(fileLoadedEvent) {
						// Save the text inside the input box's dataset
						fileInput.dataset.uploadedText = fileLoadedEvent.target.result;
						fileInput.dataset.filename = fileInput.files[0].name;

						// Show filename inside the input box
						inputBox.getElementsByClassName("custom-file-afterupload")[0].getElementsByTagName("span")[0].innerHTML = fileInput.files[0].name;

						// Show the "after upload" div
						inputBox.getElementsByClassName("custom-file-afterupload")[0].style.display = "inline-block";

						fileInput.dispatchEvent(new Event("finishupload"));
					}
					// Read the submitted file
					fileReader.readAsText(fileInput.files[0], "UTF-8");
				}
			});

			// Open a file dialog on click
			inputBox.addEventListener("click", function() {
				fileInput.click();
			});

			// Change border on file drag over
			inputBox.addEventListener("dragover", function(e) {
				inputBox.classList.add("drag");
			});

			// Remove dragover border
			inputBox.addEventListener("dragleave", function(e) {
				inputBox.classList.remove("drag");
			});
			inputBox.addEventListener("dragend", function(e) {
				inputBox.classList.remove("drag");
			});

			// Handle a file being dragged onto the uploader
			inputBox.addEventListener("drop", function(e) {
				// Prevent the browser from attempting to load the file
				e.preventDefault();
				inputBox.classList.remove("drag");

				// Append the file to the file input and trigger the "change" event
				if (e.dataTransfer.files.length > 0) {
					fileInput.files = e.dataTransfer.files;
					fileInput.dispatchEvent(new Event("change"));
				}
			});
		});
    },

    // Relies on Bootstrap for list-groups
	// TODO: add options for the search
    Autocomplete: {
        Init: function(element, items) {
            var currentFocus, shownItems;
    
            // Add "autocomplete=true" to the element's dataset
            element.dataset.autocomplete = "true";
    
            element.addEventListener("input", OnInput);
            element.addEventListener("focus", OnInput);
            element.addEventListener("focusout", function() { setTimeout(function() { NSLib.Autocomplete.CloseAllLists(); }, 100); });
    
            function OnInput(e) {
                var value = this.value;
                
                // Close any opened lists
                NSLib.Autocomplete.CloseAllLists();
    
                currentFocus = -1;
                shownItems = 0;
    
                // Create a container
                var resultContainer = document.createElement("div");
                resultContainer.id = this.id + "-autocomplete-list"
                resultContainer.classList.add("autocomplete-items");
                resultContainer.classList.add("list-group");
                resultContainer.style = "display: none; max-width: " + element.offsetWidth + "px;";
                this.parentNode.appendChild(resultContainer);

                // This will store search results where there is a match, but not at the beginning, since these results will come after the other results.
                var midResults = [];
    
                // Find items that begin with the query
                for (var i = 0; i < items.length; i++) {
					var displayName = (items[i].displayName == undefined ? items[i] : items[i].displayName);

                    // Check if the item begins with the current search query
                    var startsWith = displayName.toLowerCase().startsWith(value.toLowerCase());
                    var contains = displayName.toLowerCase().includes(value.toLowerCase()) && !(displayName.toLowerCase().startsWith(value.toLowerCase()));
                    if (startsWith || contains) {
                        resultContainer.style.display = "initial";
    
                        var result = document.createElement("div");
                        result.classList.add("list-group-item");
    
                        // Highlight the matching parts of the query in bold
                        if (startsWith) {
                            result.innerHTML = "<strong>" + displayName.substr(0, value.length) + "</strong>";
                            result.innerHTML += displayName.substr(value.length);
                        }
                        else {
                            result.innerHTML = displayName.substr(0, displayName.toLowerCase().indexOf(value.toLowerCase()));
                            result.innerHTML += "<strong>" + displayName.substr(displayName.toLowerCase().indexOf(value.toLowerCase()), value.length);
                            result.innerHTML += displayName.substr(displayName.toLowerCase().indexOf(value.toLowerCase()) + value.length);
                        }
                        
                        // Hidden input that holds the value of the item
                        result.innerHTML += "<input type='hidden' value='" + (items[i].value == undefined ? items[i] : items[i].value) + "'>";
                        
                        result.addEventListener("click", function(e) {
                            // Fill out text box with the value of the selected option
                            element.value = this.getElementsByTagName("input")[0].value;
                            
                            // Close opened lists
                            NSLib.Autocomplete.CloseAllLists();
                        });

                        if (startsWith) {
                            resultContainer.appendChild(result);
                            shownItems++;
                        }
                        else
                            midResults.push(result);
                    }
                }

                midResults.forEach(result => {
                    resultContainer.appendChild(result);
                    shownItems++;
                });
            }
    
            // Handle arrow keys and enter
            element.addEventListener("keydown", function(e) {
                var item = document.getElementById(this.id + "-autocomplete-list");
    
                if (item)
                    item = item.getElementsByTagName("div");
    
                // Down arrow
                if (e.keyCode == 40) {
                    if (currentFocus != (shownItems - 1)) {
                        currentFocus++;
                        NSLib.Autocomplete.AddActiveFormat(item, currentFocus);
                    }
                    else {
                        currentFocus = 0;
                        NSLib.Autocomplete.AddActiveFormat(item, currentFocus);
                    }
                }
                // Up arrow
                else if (e.keyCode == 38) {
                    if (currentFocus != 0) {
                        currentFocus--;
                        NSLib.Autocomplete.AddActiveFormat(item, currentFocus);
                    }
                    else {
                        currentFocus = shownItems - 1;
                        NSLib.Autocomplete.AddActiveFormat(item, currentFocus);
                    }
                }
                // Enter
                else if (e.keyCode == 13) {
                    e.preventDefault();
    
                    if (currentFocus > -1) {
                        if (item) item[currentFocus].click();
                    }
                }
            });
        },
    
        // Adds "active" formatting
        AddActiveFormat: function(item, currentFocus) {
            if (!item)
                return false;
        
            NSLib.Autocomplete.RemoveActiveFormat(item);
    
            if (currentFocus >= item.length)
                currentFocus = 0;
    
            if (currentFocus < 0)
                currentFocus = (item.length - 1);
    
            item[currentFocus].classList.add("active");
    
            // Scroll to appropriate position
            document.getElementById(item[currentFocus].parentNode.id).scrollTop = item[currentFocus].offsetTop - 80;
        },
    
        // Removes "active" formatting
        RemoveActiveFormat: function(item) {
            for (var i = 0; i < item.length; i++) {
                item[i].classList.remove("active");
            }
        },
    
        // Closes all autocomplete lists
        CloseAllLists: function() {
            var resultContainer = document.getElementsByClassName("autocomplete-items");
            for (var i = 0; i < resultContainer.length; i++) {
                resultContainer[i].parentNode.removeChild(resultContainer[i]);
            }
        }
    },

	// Downloads a text file with the specified filename and contents
	DownloadFile: function(filename, contents) {
		var element = document.createElement("a");
		element.setAttribute("href", "data:text/plain;charset=utf-8," + encodeURIComponent(contents));
		element.setAttribute("download", filename);

		element.style.display = "none";
		document.body.appendChild(element);

		element.click();

		document.body.removeChild(element);
	},

	Toasts: {
		Setup: function(alignTop = true, alignRight = true) {
			if (document.getElementById("toast-container") != null)
				document.getElementById("toast-container").remove();

			var toastContainer = document.createElement("div");
			toastContainer.id = "toast-container";

			toastContainer.style.zIndex = 100000;
			toastContainer.style.position = "fixed";

			// Vertical alignment
			if (alignTop)
				toastContainer.style.top = "5px";
			else
				toastContainer.style.bottom = "5px";

			// Horizontal alignment
			if (alignRight)
				toastContainer.style.right = "5px";
			else
				toastContainer.style.left = "5px";

			document.body.appendChild(toastContainer);
		},

		Show: function(color, icon, title, message, timeout) {
			var toastId = Date.now();
			document.getElementById("toast-container").innerHTML += `
			<div id="toast-` + toastId + `" class="toast" data-bs-delay="` + timeout + `" role="alert">
				<div class="toast-header bg-` + color + `">
					<i class="bi-` + icon + `"></i>&nbsp;&nbsp;
					<strong class="me-auto">` + title + `</strong>
					<button type="button" class="btn-close" data-bs-dismiss="toast"></button>
				</div>
				<div class="toast-body" id="toast-body-` + toastId + `">` + message + `</div>
			</div>`;

			var toast = new bootstrap.Toast(document.getElementById("toast-" + toastId));
			toast.show();

			document.getElementById("toast-" + toastId).addEventListener("hidden.bs.toast", function() {
				setTimeout(function() {
					if (document.getElementById("toast-" + toastId) != null)
						document.getElementById("toast-" + toastId).remove();
				}, 500);
			});
		},

		ShowPrompt: function(color, icon, title, message, onYes, onNo, yesButtonColor, noButtonColor) {
			var toastId = Date.now();
			document.getElementById("toast-container").innerHTML += `
			<div id="toast-` + toastId + `" class="toast" role="alert" data-bs-autohide="false">
				<div class="toast-header bg-` + color + `">
					<i class="bi-` + icon + `"></i>&nbsp;&nbsp;
					<strong class="me-auto">` + title + `</strong>
					<button type="button" class="btn-close" data-bs-dismiss="toast"></button>
				</div>
				<div class="toast-body" id="toast-body-` + toastId + `">` + message + `
                    <div style="margin-top: 10px;">
                        <button class="btn btn-` + noButtonColor + `" data-bs-dismiss="toast" id="toast-` + toastId + `-no">No</button>
                        <button class="btn btn-` + yesButtonColor + `" data-bs-dismiss="toast" id="toast-` + toastId + `-yes">Yes</button>
                    </div>
                </div>
			</div>`;

            document.getElementById("toast-" + toastId + "-yes").addEventListener("click", onYes);
            document.getElementById("toast-" + toastId + "-no").addEventListener("click", onNo);

			var toast = new bootstrap.Toast(document.getElementById("toast-" + toastId));
			toast.show();
		}
	}
}

class Table {
	static SortTypes = {
		"None": 0,
		"Descending": 1,
		"Ascending": 2
	}

	static SearchModes = {
		"Normal": 0,
		"Strict": 1,
		"Lenient": 2
	}

	//#region Define variables
		// The HTML DOM element that contains the table
		element;

		// Columns and their options
		columns;

		// Rows in the table
		rows = [];

		// Tracks the current page number
		#page = 0;
		
		// Current options as specified by thee user
		options;

		// Rows that match the current filter
		#filteredRows = [];

		// Tracks which column is being used for sorts
		#sortedColumn = undefined;

		// Tracks the number of hidden rows
		#hiddenRowCount = 0;
	//#endregion

	constructor(element, columns, options = {}) {
		var self = this;

		//#region Set variable values
			this.element = element;
			this.columns = columns;
			this.options = options;
		//#endregion

		//#region Set default values for undefined options
            // TODO: add an option to remove the "select all rows" checkbox
			this.options.rowPerPageOptions = (this.options.rowPerPageOptions || [5, 10, 15, 25, 50, 75, 100]);
			this.options.rowsPerPage = this.options.rowsPerPage || (this.options.rowPerPageOptions != undefined ? this.options.rowPerPageOptions[0] : 15);
			this.options.sortType = (this.options.sortType || Table.SortTypes.None);
			this.options.searchMode = (this.options.searchMode || Table.SearchModes.Normal);
			this.options.checkboxes = (this.options.checkboxes || false);
			this.options.color = (this.options.color || "light");
			this.options.alternateColor = (this.options.alternateColor || "default");
			this.options.rowHover = (this.options.rowHover == undefined ? true : this.options.rowHover);
			this.options.onDisplayRows = (this.options.onDisplayRows == undefined ? null : this.options.onDisplayRows);
		//#endregion

		//#region Pre-Table elements
            // TODO: can I make the layout work better if the table is missing some bits at the header (e.g. title, actions button, filter, etc.)
			element.classList.add("nslib-table");

			if (options.sticky)
				element.classList.add("nslib-table-sticky");

			if (options.rowHover)
				element.classList.add("nslib-table-rowhover");

			// Remove the outline if requested
			if (options.outline == false)
				element.classList.add("nslib-table-nooutline");

			// Add a <h4> title if one has been specified
			if (options.title != "" && options.title != undefined) {
				var tableHeader = document.createElement("h4");
				tableHeader.classList = "nslib-table-header"
				tableHeader.innerHTML = options.title;
				element.appendChild(tableHeader);
			}

			// Add a search box if requested
			if (options.search) {
				this.#AddSearchBox();
			}

			// Add an Actions dropdown if checkboxes are enabled and actions have been specified
			if (this.options.actions != undefined && this.options.actions.length > 0) {
				this.#AddActions();
			}
		//#endregion

		//#region Set up table
			var tableWrapper = document.createElement("div");
			tableWrapper.classList = "nslib-table-wrapper";

			var table = document.createElement("table");

			table.classList = "table table-striped table-" + this.options.color;
			
			if (this.options.alternateColor != "default")
				table.style = "--bs-table-striped-bg: " + this.options.alternateColor;

			var tr = document.createElement("tr")

			// Add a master checkbox cell if checkboxes are enabled
			if (this.options.checkboxes) {
				tr = this.#CreateMasterCheckbox(tr);
			}

			// Create a header cell for each column
			for (let i = 0; i < columns.length; i++) {
				var column = columns[i];

				var th = document.createElement("th");
				th.scope = "col";

				var headerText = document.createElement("span");
				headerText.innerHTML = column.name;

				th.style.minWidth = (column.minWidth || "inherit");
				th.style.maxWidth = (column.maxWidth || "inherit");
				th.style.width = (column.width || "inherit");
				th.style.textAlign = (column.textAlign || "left");

				// If the column is sortable, add a sort icons and make the text clickable
				if (column.sortable) {
					th.dataset.columnId = i;

					headerText.classList.add("nslib-table-span-sortable");
					headerText.onclick = function() {
						self.Sort(this.parentElement.dataset.columnId);
					}
					th.appendChild(headerText);

					var sortIcon = document.createElement("button");
					sortIcon.type = "button";
					sortIcon.classList = "nslib-table-iconbtn btn";
					sortIcon.innerHTML = `<i class="bi-arrow-down-up"></i>`;
					sortIcon.onclick = function() {
						self.Sort(this.parentElement.dataset.columnId);
					}
					th.appendChild(sortIcon);
				}
				else
					th.appendChild(headerText);

				// If the column is filterable, add a filter button
				if (column.filter != undefined && column.filter != false) {
					var dropdownDiv = document.createElement("div");
					dropdownDiv.classList = "dropdown";
					dropdownDiv.style.display = "inline";

					var filterIcon = document.createElement("button");
					filterIcon.type = "button";
					filterIcon.classList = "nslib-table-iconbtn btn";
					filterIcon.dataset.bsToggle = "dropdown";
					filterIcon.dataset.bsAutoClose = "outside";
					filterIcon.innerHTML = `<i class="bi-funnel"></i>`;

					var filterMenu = document.createElement("div");
					filterMenu.classList = "dropdown-menu nslib-table-filter-dropdown";
					filterMenu.style.fontWeight = "normal";
					filterMenu.style.inset = 0;
					filterMenu.style.padding = "10px";
					this.columns[i].filterMenu = filterMenu;
					this.columns[i].filterValues = [{"shown": true, "value": ""}];
					this.columns[i].lastRowValueId = 0;

					dropdownDiv.appendChild(filterIcon);
					dropdownDiv.appendChild(filterMenu);

					var formCheckDiv = document.createElement("div");
					formCheckDiv.classList = "form-check";

					let checkbox = document.createElement("input");
					checkbox.classList = "form-check-input";
					checkbox.type = "checkbox";
					checkbox.id = "nslib-table-" + this.id + "col-" + i + "-value-0";
					checkbox.checked = true;
					var self = this;
					checkbox.addEventListener("input", function() {
						self.columns[i].filterValues[0].shown = checkbox.checked;
						self.#DisplayRows();
					});

					var label = document.createElement("label");
					label.classList = "form-check-label";
					label.htmlFor = checkbox.id;
					label.innerHTML = "(Blanks)";

					formCheckDiv.appendChild(checkbox);
					formCheckDiv.appendChild(label);
					this.columns[i].filterMenu.appendChild(formCheckDiv);

					th.appendChild(dropdownDiv);
					th.style.zIndex = 3;
				}

				tr.appendChild(th);
			}

			var thead = document.createElement("thead");
			thead.appendChild(tr);
			table.appendChild(thead);

			var tbody = document.createElement("tbody");
			table.appendChild(tbody);

			tableWrapper.appendChild(table);
			element.appendChild(tableWrapper);
		//#endregion

		//#region Row count indicator
			var rowCount = document.createElement("span");
			rowCount.classList = "nslib-table-rowcount list-group-item";
			rowCount.innerHTML = "Showing 0 of 0 rows";
			element.appendChild(rowCount);
		//#endregion

		if (this.options.rowPerPageOptions.length > 1)
			this.#AddRowsPerPageDropdown();

		if (this.options.allowHidingColumns == true)
			this.#AddColumnHider();

		this.#AddPageNavButtons();
	}

	// Adds a search box to the table
	#AddSearchBox() {
		var self = this;

		var searchBoxContainer = document.createElement("div");
		searchBoxContainer.classList = "nslib-table-searchbox floating-labels icon-textbox";

		var searchIcon = document.createElement("i");
		searchIcon.classList = "bi-search icon nslib-table-searchbox-icon";
		searchBoxContainer.appendChild(searchIcon);

		var searchBox = document.createElement("input");
		searchBox.type = "text";
		searchBox.classList.add("form-control");
		searchBox.placeholder = "";
		searchBox.oninput = function() {
			self.#Search(this.value);
			self.#DisplayRows();
		}
		searchBoxContainer.appendChild(searchBox);

		var label = document.createElement("label");
		label.innerHTML += "Filter";
		searchBoxContainer.appendChild(label);

		this.element.appendChild(searchBoxContainer);
	}

	// Adds an Actions button to the table
	#AddActions() {
		var self = this;
		
		// Create a dropdown button
		var div = document.createElement("div");
		div.classList = "nslib-table-actions dropdown";

		var button = document.createElement("button");
		button.type = "button";
		button.classList = "btn btn-secondary dropdown-toggle";
		button.dataset.bsToggle = "dropdown";
		button.innerHTML = "Actions";

		var menu = document.createElement("ul");
		menu.classList = "dropdown-menu";

		this.options.actions.forEach(action => {
			// Hiding
			if (action.type == "hide") {
                // TODO: if you press the "hide" button with 0 rows selected, you will get a banner saying "0 row is hidden"
				var hideButtonLi = document.createElement("li");
				var hideButtonA = document.createElement("a");
				hideButtonA.classList = "dropdown-item";
				hideButtonA.innerHTML = "Hide selected rows";
				hideButtonA.onclick = function() {
					self.#filteredRows.forEach(row => {
						if (self.rows[row.id].checked) {
							self.rows[row.id].hidden = true;
							self.#hiddenRowCount++;
						}
					});
					self.#DisplayRows();

					// Show a banner alert if rows have been hidden and the feature has not been disabled
					if (action.showAlert != false) {
						// Remove the old alert if there is one
						if (self.element.getElementsByClassName("nslib-table-hiddenalert").length > 0) {
							self.element.getElementsByClassName("nslib-table-hiddenalert")[0].remove();
						}

						// It is inside a setTimeout function so that the menu has a chance to hide itself before the alert pops up
						setTimeout(function() {
							var alertDiv = document.createElement("div");
							alertDiv.classList = "alert alert-info alert-dismissible nslib-table-hiddenalert";
							alertDiv.role = "alert";
							alertDiv.innerHTML = `<i class="bi-info-circle"></i>&nbsp;&nbsp;` + self.#hiddenRowCount + (self.#hiddenRowCount > 1 ? " rows are hidden" : " row is hidden") + ".&nbsp;";

							var link = document.createElement("a");
							link.classList = "link-dark";
							link.style.cursor = "pointer";
							link.innerHTML = "Click here to show all rows";
							link.onclick = function() {
								self.#hiddenRowCount = 0;

								for (var i = 0; i < self.rows.length; i++) {
									self.rows[i].hidden = false;
								}
								self.#DisplayRows();
								
								if (self.element.getElementsByClassName("nslib-table-hiddenalert").length > 0)
									self.element.getElementsByClassName("nslib-table-hiddenalert")[0].remove();
							};
							alertDiv.appendChild(link);

							var button = document.createElement("button");
							button.classList = "btn-close";
							button.dataset.bsDismiss = "alert";
							alertDiv.appendChild(button);

							self.element.prepend(alertDiv);
						}, 0);
					}
				};
				hideButtonLi.appendChild(hideButtonA);
				menu.appendChild(hideButtonLi);
	
				var showButtonLi = document.createElement("li");
				var showButtonA = document.createElement("a");
				showButtonA.classList = "dropdown-item";
				showButtonA.innerHTML = "Restore hidden rows";
				showButtonA.onclick = function() {
					self.#hiddenRowCount = 0;

					for (var i = 0; i < self.rows.length; i++) {
						self.rows[i].hidden = false;
					}
					self.#DisplayRows();
					
					if (self.element.getElementsByClassName("nslib-table-hiddenalert").length > 0)
						self.element.getElementsByClassName("nslib-table-hiddenalert")[0].remove();
				};
				showButtonLi.appendChild(showButtonA);
				menu.appendChild(showButtonLi);
			}

			// CSV and JSON exports
			else if (action.type == "exportCSV" || action.type == "exportJSON") {
				var exportButtonLi = document.createElement("li");
				var exportButtonA = document.createElement("a");
				exportButtonA.classList = "dropdown-item";
				exportButtonA.innerHTML = "Export selected rows to " + (action.type == "exportJSON" ? "JSON" : "CSV");
				
				if (action.type == "exportCSV") {
					exportButtonA.addEventListener("click", function() {
						var csvText = "";
						var exportables = [];
						self.columns.forEach(col => {
							if (col.exportable) {
								if (col.name.toString().includes(","))
									csvText += "\"" + col.name.toString().replaceAll("\"", "\\\"") + "\",";
								else
									csvText += col.name.toString() + ",";
							}
		
							exportables.push(col.exportable);
						});
						csvText = csvText.replace(/,$/, "\n");
		
						self.#filteredRows.forEach(row => {
							if (row.checked) {
								for (var i = 0; i < row.cells.length; i++) {
									if (exportables[i]) {
										if (row.cells[i].toString().includes(","))										
											csvText += "\"" + row.cells[i].toString().replaceAll("\"", "\\\"") + "\",";
										else
											csvText += row.cells[i].toString() + ",";
									}
								}
								csvText = csvText.replace(/,$/, "\n");
							}
						});
		
						NSLib.DownloadFile((action.filename || "Export.csv"), csvText);
					});
				}
				else {
                    // TODO: this will export all columns and not just the ones that have been set as selectable
					exportButtonA.addEventListener("click", function() {
						NSLib.DownloadFile((action.filename || "Export.json"), JSON.stringify(self.GetCheckedRows()));
					});
				}

				exportButtonLi.appendChild(exportButtonA);
				menu.appendChild(exportButtonLi);
			}

			// Custom user-defined actions
			else if (action.type == "custom") {
				var customButtonLi = document.createElement("li");
				var customButtonA = document.createElement("a");
				customButtonA.classList = "dropdown-item";
				customButtonA.innerHTML = action.name;
				customButtonA.onclick = function() {
					action.function(self.GetCheckedRows());
				};
				customButtonLi.appendChild(customButtonA);
				menu.appendChild(customButtonLi);
			}
		});

		div.appendChild(button)
		div.appendChild(menu);
		this.element.appendChild(div);
	}

	// Creates a "master checkbox" cell in the header column that allows you to select all rows
	#CreateMasterCheckbox(tr) {
		var self = this;

		var th = document.createElement("th");
		th.scope = "col";
		th.style.width = "40px";

		var checkbox = document.createElement("input");
		checkbox.type = "checkbox";
		checkbox.classList = "form-check-input nslib-table-mastercheckbox";
		checkbox.oninput = function() {					
			for (let checkbox of self.element.getElementsByClassName("nslib-table-checkbox")) {
				checkbox.checked = this.checked;
			}

			self.#filteredRows.forEach(row => {
				row.checked = this.checked;
			});
		}

		th.appendChild(checkbox);
		tr.appendChild(th);

		return tr;
	}

	// Adds the specified rows to the table and displays them
	AddRows(rows) {
		rows.forEach(row => {
			this.rows.push({
				"id": this.rows.length,
				"cells": row,
				"match": true,
				"checked": false,
				"hidden": false,
				"hiddenByColumnFilter": false
			});

			for (let i = 0; i < row.length; i++) {
				if (this.columns[i].filter) {
					var found = false;
					this.columns[i].filterValues.forEach(value => {
						if (value["value"] == row[i])
							found = true;
					});
					if (found == false) {
						var self = this;

						this.columns[i].filterValues.push({"shown": true, "value": row[i]});
						let filterValueIndex = (this.columns[i].filterValues.length - 1);

						var formCheckDiv = document.createElement("div");
						formCheckDiv.classList = "form-check";

						let checkbox = document.createElement("input");
						checkbox.classList = "form-check-input";
						checkbox.type = "checkbox";
						this.columns[i].lastRowValueId++;
						checkbox.id = "nslib-table-" + this.id + "col-" + i + "-value-" + this.columns[i].lastRowValueId;
						checkbox.checked = true;
						checkbox.addEventListener("input", function() {
							self.columns[i].filterValues[filterValueIndex].shown = checkbox.checked;
							self.#DisplayRows();
						});

						var label = document.createElement("label");
						label.classList = "form-check-label";
						label.htmlFor = checkbox.id;
						label.innerHTML = row[i];

						formCheckDiv.appendChild(checkbox);
						formCheckDiv.appendChild(label);
						this.columns[i].filterMenu.appendChild(formCheckDiv);
					}
				}
			}
		});

		if (this.options.search == true)
			this.#Search(this.element.getElementsByClassName("nslib-table-searchbox")[0].getElementsByTagName("input")[0].value);
		this.#DisplayRows();
	}

	// Updates the displayed rows
	#DisplayRows() {
		var self = this;

		var tbody = this.element.getElementsByTagName("tbody")[0];
		tbody.innerHTML = "";

		var shownRows = 0;

		this.UncheckAll();

		// Go through rows and determine which ones should be hidden by a column filter
		for (var r = 0; r < this.rows.length; r++)
			this.rows[r].hiddenByColumnFilter = false;
		for (var c = 0; c < this.columns.length; c++) {
			if (this.columns[c].filter) {
				for (var r = 0; r < this.rows.length; r++) {
					this.columns[c].filterValues.forEach(value => {
						if (this.rows[r].cells[c] == value["value"] && value["shown"] == false)
							this.rows[r].hiddenByColumnFilter = true;
					});
				}
			}
		}

		this.#filteredRows = this.rows.filter(row => {
			return (row.match == true && row.hidden == false && row.hiddenByColumnFilter == false);
		});

		if (this.#filteredRows.length < this.options.rowsPerPage)
			this.element.getElementsByClassName("nslib-table-navbuttons")[0].style.display = "none";
		else
			this.element.getElementsByClassName("nslib-table-navbuttons")[0].style.display = "";

		// Starts at (page * maxRows) - the first row that needs to be displayed - and keeps going until it has displayed this.options.rowsPerPage rows
		for (var i = (this.#page * this.options.rowsPerPage); i < ((this.#page * this.options.rowsPerPage) + this.options.rowsPerPage); i++) {
			if (this.#filteredRows[i] != undefined) {				
				var tr = document.createElement("tr");

				tr.dataset.index = this.#filteredRows[i].id;

				// Add a checkbox cell if checkboxes are enabled
				if (this.options.checkboxes) {
					var td = document.createElement("td");

					// Make the <td> click on the checkbox if clicked
					td.style.cursor = "pointer";
					td.onclick = function(e) {
						if (this.getElementsByClassName("nslib-table-checkbox")[0] != undefined && e.target === e.currentTarget)
							this.getElementsByClassName("nslib-table-checkbox")[0].click();
					}

					var checkbox = document.createElement("input");
					checkbox.type = "checkbox";
					checkbox.classList = "form-check-input nslib-table-checkbox";
					checkbox.dataset.index = this.#filteredRows[i]["id"];
					checkbox.oninput = function() {
						self.rows.forEach(row => {
							if (row.id == parseInt(this.dataset.index))
								row.checked = this.checked;
						})
					}

					td.appendChild(checkbox);
					tr.appendChild(td);
				}

				// Add row cells
				for (let j = 0; j < this.#filteredRows[i].cells.length; j++) {
					var td = document.createElement(this.columns[j].header == true ? "th" : "td");
					td.style.textAlign = this.columns[j].textAlign;
					td.innerHTML = "<span class='nslib-table-cell'>" + this.#filteredRows[i].cells[j] + "</span>";

					// Allow editing if it is enabled for the column
					// TODO: can we move this to a different function?
					if (this.columns[j].editable) {
						td.dataset.id = this.#filteredRows[i].id;
						td.dataset.index = j;

						var editButton = document.createElement("button");
						editButton.type = "button";
						editButton.classList = "nslib-table-iconbtn btn";
						editButton.innerHTML = `<i class="bi-pencil"></i>`;
						editButton.onclick = function() {
							 if (this.parentElement.getElementsByClassName("nslib-table-cell").length > 0) {
								var textbox = document.createElement("input");
								textbox.type = "text";
								textbox.classList = "nslib-table-editbox form-control";
								textbox.value = this.parentElement.getElementsByClassName("nslib-table-cell")[0].innerText;
								this.parentElement.prepend(textbox);

								this.parentElement.getElementsByClassName("nslib-table-cell")[0].remove();
							}
							else if (this.parentElement.getElementsByClassName("nslib-table-editbox").length > 0) {
								var newText = this.parentElement.getElementsByClassName("nslib-table-editbox")[0].value;

								for (var i = 0; i < self.rows.length; i++) {
									if (self.rows[i].id == parseInt(this.parentElement.dataset.id))
										self.rows[i].cells[parseInt(this.parentElement.dataset.index)] = newText;
								}

								var span = document.createElement("span");
								span.classList = "nslib-table-cell";
								span.innerText = newText;
								this.parentElement.getElementsByClassName("nslib-table-editbox")[0].remove();
								this.parentElement.prepend(span);
								
								if (self.columns[j].onedit != undefined) {
									self.columns[j].onedit(parseInt(this.parentElement.parentElement.dataset.index));
								}
							}
						}
						td.appendChild(editButton);
					}

					tr.appendChild(td);
				}
				tbody.appendChild(tr);

				shownRows++;
			}
		}

		// Determine if the previous and next buttons should be enabled or disabled
		if (this.options.rowsPerPage < this.#filteredRows.length && this.#page < (this.#filteredRows.length / this.options.rowsPerPage - 1))
			this.element.getElementsByClassName("nslib-table-navbuttons")[0].getElementsByTagName("button")[1].classList.remove("disabled");
		else
			this.element.getElementsByClassName("nslib-table-navbuttons")[0].getElementsByTagName("button")[1].classList.add("disabled");
		if (this.#page > 0)
			this.element.getElementsByClassName("nslib-table-navbuttons")[0].getElementsByTagName("button")[0].classList.remove("disabled");
		else
			this.element.getElementsByClassName("nslib-table-navbuttons")[0].getElementsByTagName("button")[0].classList.add("disabled");

		// Update the row count indicator
		this.element.getElementsByClassName("nslib-table-rowcount")[0].innerHTML = 
		"Showing " + (this.#filteredRows.length <= 1 ? this.#filteredRows.length : ((this.#page * this.options.rowsPerPage + 1) || 0) + "-" +
		Math.min(this.#filteredRows.length, ((this.#page * this.options.rowsPerPage + this.options.rowsPerPage) || 0)))
		+ " of " + this.#filteredRows.length + " rows";

		var table = self.element.getElementsByTagName("table")[0];
		for (var i = 0; i < this.columns.length; i++) {
			for (let tr of table.getElementsByTagName("tr")) {
				if (tr.getElementsByTagName("td").length > 0)
					tr.getElementsByTagName("td")[i].style.display = (this.columns[i].shown != false ? "revert" : "none");
				if (tr.getElementsByTagName("th").length > 0)
					tr.getElementsByTagName("th")[i].style.display = (this.columns[i].shown != false ? "revert" : "none");
			}
		}

		// Run user hook
		try {
			this.options.onDisplayRows();
		} catch(e) {}
	}

	// Adds a dropdown that lets the user select which columns should be displayed
	#AddColumnHider() {
		var self = this;
		var shownColumns = 0;
		this.columns.forEach(column => {
			if (column.shown != false)
				shownColumns++;
		});

		// Container for the dropdown button and the dropdown menu
		var columnHider = document.createElement("div");
		columnHider.classList ="dropdown nslib-table-rowcountselect";

		// Opens the dropdown menu
		var dropdownButton = document.createElement("button");
		dropdownButton.type = "button";
		dropdownButton.classList = "btn page-link dropdown-toggle nslib-table-rowcount-dropdownbutton";
		dropdownButton.dataset.bsToggle = "dropdown";
		dropdownButton.innerHTML = "Showing " + shownColumns + " of " + this.columns.length + " columns";
		dropdownButton.dataset.bsAutoClose = "outside";

		var dropdownMenu = document.createElement("div");
		dropdownMenu.classList = "dropdown-menu nslib-columnhider-menu";

		for (let i = 0; i < this.columns.length; i++) {
			var formCheckDiv = document.createElement("div");
			formCheckDiv.classList = "form-check";
			let checkbox = document.createElement("input");
			checkbox.classList = "form-check-input";
			checkbox.type = "checkbox";
			checkbox.name = "nslib-table-" + this.element.id + "-columns";
			checkbox.checked = (this.columns[i].shown != false);
			checkbox.id = "nslib-table-" + this.element.id + "-column-check-" + i;
			if (this.columns[i].forceShown == true)
				checkbox.disabled = true;
			var label = document.createElement("label");
			label.classList = "form-check-label"
			label.innerHTML = this.columns[i].name;
			label.htmlFor = "nslib-table-" + this.element.id + "-column-check-" + i;
			formCheckDiv.appendChild(checkbox);
			formCheckDiv.appendChild(label);
			dropdownMenu.appendChild(formCheckDiv);

			checkbox.addEventListener("input", function() {
				self.columns[i].shown = checkbox.checked;
				self.#DisplayRows();
				shownColumns = (checkbox.checked ? shownColumns + 1 : shownColumns - 1);
				dropdownButton.innerHTML = "Showing " + shownColumns + " of " + self.columns.length + " columns";
			});
		}

		columnHider.appendChild(dropdownButton);
		columnHider.appendChild(dropdownMenu);
		this.element.appendChild(columnHider);
	}

	// Adds a dropdown that lets the user select how many rows are shown per page
	#AddRowsPerPageDropdown() {
		var self = this;

		// Container for the dropdown button and the dropdown menu
		var rowCountSelect = document.createElement("div");
		rowCountSelect.classList ="dropdown nslib-table-rowcountselect";

		// Opens the dropdown menu
		var dropdownButton = document.createElement("button");
		dropdownButton.type = "button";
		dropdownButton.classList = "btn page-link dropdown-toggle nslib-table-rowcount-dropdownbutton";
		dropdownButton.dataset.bsToggle = "dropdown";
		dropdownButton.innerHTML = "Show " + self.options.rowsPerPage + " rows";

		// Lets the user select the number of rows per page
		var dropdownMenu = document.createElement("ul");
		dropdownMenu.classList = "dropdown-menu";
		self.options.rowPerPageOptions.forEach(option => {
			var li = document.createElement("li");
			var a = document.createElement("a");

			a.classList = "dropdown-item";
			a.onclick = function() {
				self.page = 0;
				self.options.rowsPerPage = option;
				this.parentElement.parentElement.parentElement.parentElement.getElementsByClassName("nslib-table-rowcount-dropdownbutton")[0].innerHTML = "Show " + option + " rows";
				self.#DisplayRows();
			}
			a.innerHTML = option;
			li.appendChild(a);
			dropdownMenu.appendChild(li);
		});

		rowCountSelect.appendChild(dropdownButton);
		rowCountSelect.appendChild(dropdownMenu);
		this.element.appendChild(rowCountSelect);
	}

	// Adds previous and next buttons to the table
	#AddPageNavButtons() {
		var self = this;

		var navButtonContainer = document.createElement("div");
		navButtonContainer.classList = "nslib-table-navbuttons"

		var prevButton = document.createElement("button");
		prevButton.classList = "nslib-table-prevnext btn";
		prevButton.type = "button";
		prevButton.innerHTML = "Previous";
		prevButton.onclick = function() {
			self.Previous();
		};
		navButtonContainer.appendChild(prevButton);

		var nextButton = document.createElement("button");
		nextButton.type = "button";
		nextButton.classList = "nslib-table-prevnext btn";
		nextButton.innerHTML = "Next";
		nextButton.onclick = function() {
			self.Next();
		};
		navButtonContainer.appendChild(nextButton);

		this.element.appendChild(navButtonContainer);
	}

	GetCheckedRows() {
		var self = this;

		var checkedRowJson = [];
		var cols = [];
		for (const [key, value] of Object.entries(self.columns)) {
			cols.push(value.name);
		}

		self.rows.forEach(row => {
			if (row.checked) {
				var rowJson = {};

				for (var i = 0; i < self.columns.length; i++) {
					rowJson[self.columns[i].name.toString()] = (row.cells[i].toString() || "");
				}

				checkedRowJson.push(rowJson);
			}
		});

		return checkedRowJson;
	}

	// Navigate to the previous page
	Previous() {
		if (this.#page > 0) {
			this.#page--;
			this.#DisplayRows();
		}
	}

	// Navigate to the next page
	Next() {
		if (this.options.rowsPerPage < this.#filteredRows.length && this.#page < (this.#filteredRows.length / this.options.rowsPerPage - 1)) {
			this.#page++;
			this.#DisplayRows();
		}
	}

	// Searches the rows for the specified query
	#Search(query) {
		this.#page = 0;

		// Array of booleans that specify which columns can be sorted
		var searchableCols = [];
		this.columns.forEach(column => {
			searchableCols.push(column.searchable == true);
		});

		// This must be done with a for and not a forEach so we can have a reference to the row itself and not a copy of the row
		for (var i = 0; i < this.rows.length; i++) {
			var match = false;

			// TODO: change the search to give you options instead of 3 presets
			// The user would be able to specify if the search should match case, ignore whitespace,
			// match across columns, and etc.

			// Also add a substitute regex option that is per column
			// For example: if value is (555) 123-456, you could feed the regex [^0-9]
			// and the program would automatically remove anything that isn't a number
			// from both the query and the value when searching that column

			if (this.options.searchMode != Table.SearchModes.Lenient) {
				for (var j = 0; j < this.rows[i].cells.length; j++) {
					var lenientSearch = "";

					if (searchableCols[j]) {
						if (this.options.searchMode == Table.SearchModes.Normal && this.rows[i].cells[j].toLowerCase().includes(query.toLowerCase()))
							match = true;
						else if (this.options.searchMode == Table.SearchModes.Strict && this.rows[i].cells[j].includes(query))
							match = true;
						else if (this.options.searchMode == Table.SearchModes.Lenient)
							lenientSearch += this.rows[i].cells[j].toLowerCase().replaceAll(/(\s)|(\t)/g, "");
					}

					if (this.options.searchMode == Table.SearchModes.Lenient && lenientSearch.includes(query.toLowerCase().replaceAll(/(\s)|(\t)/g, "")))
						match = true;
				}
			}
			else {
				var allCells = "";
				for (var j = 0; j < this.rows[i].cells.length; j++) {
					if (searchableCols[j])
						allCells += this.rows[i].cells[j];
				}

				match = (allCells.toLowerCase().replaceAll(/(\s)|(\t)/g, "").includes(query.toLowerCase().replaceAll(/(\s)|(\t)/g, "")));
			}

			this.rows[i].match = match;
		}
	}

	// Resets the table
	Clear(resetSearch = true) {
		if (this.options.search == true && resetSearch)
			this.element.getElementsByClassName("nslib-table-searchbox")[0].getElementsByTagName("input")[0].value = "";
		this.rows = [];
		this.#page = 0;
		this.element.getElementsByTagName("tbody")[0].innerHTML = "";
	}

	// Applies a sort on the table rows
	Sort(columnId) {
		this.UncheckAll();

		// Determine the sort settings
		if (this.#sortedColumn == columnId) {
			this.options.sortType++;

			if (this.options.sortType == 3)
				this.options.sortType = 0;
		}
		else {
			this.#sortedColumn = columnId;
			this.options.sortType = 1;
		}

		// Change the icon of the sort icon inside the column we are sorting and reset all other sort icons
		for (let colElement of this.element.getElementsByTagName("thead")[0].getElementsByTagName("th")) {
			if (colElement.getElementsByClassName("nslib-table-iconbtn").length > 0) {
				var classList = colElement.getElementsByClassName("nslib-table-iconbtn")[0].getElementsByTagName("i")[0].classList;
				if (classList.contains("bi-arrow-up") || classList.contains("bi-arrow-down"))
					colElement.getElementsByClassName("nslib-table-iconbtn")[0].getElementsByTagName("i")[0].classList = "bi-arrow-down-up";
			}

			if (parseInt(colElement.dataset.columnId) == columnId) {
				if (this.options.sortType == 1)
					colElement.getElementsByClassName("nslib-table-iconbtn")[0].getElementsByTagName("i")[0].classList = "bi-arrow-up";
				else if (this.options.sortType == 2)
					colElement.getElementsByClassName("nslib-table-iconbtn")[0].getElementsByTagName("i")[0].classList = "bi-arrow-down";
			}
		}
		
		// Used for string comparison
		var collator = new Intl.Collator(undefined, { "numeric": true, "sensitivity": "base" });

		// Sort in descending order
		if (this.options.sortType == 1) {
			this.rows.sort(function(a, b) {
				return collator.compare(a.cells[columnId], b.cells[columnId]);
			});
		}
		// Sort in ascending order
		else if (this.options.sortType == 2) {
			this.rows.sort(function(a, b) {
				return collator.compare(b.cells[columnId], a.cells[columnId]);
			});
		}
		// Sort in original order
		else {
			this.rows.sort(function(a, b) {
				return collator.compare(a.id, b.id);
			});
		}

		this.#DisplayRows();
	}

	// Unchecks all checkboxes in the table
	UncheckAll() {
		for (var i = 0; i < this.rows.length; i++) {
			if (this.element.getElementsByClassName("form-check-input nslib-table-mastercheckbox")[0] != undefined)
				this.element.getElementsByClassName("form-check-input nslib-table-mastercheckbox")[0].checked = false;
			this.rows[i].checked = false;
		}
	}

	GetRow(rowIndex) {
		for (var i = 0; i < this.rows.length; i++) {
			if (this.rows[i].id == rowIndex) {
				return this.rows[i].cells;
			}
		}
		return undefined;
	}
}

class ContextMenu {
    // TODO: could this be improved using bootstrap dropdowns? Submenus could be created by making a sub-dropdown with data-bs-trigger=hover
	constructor(element, options) {
		var self = this;
		this.options = options;
		this.element = element;

		element.addEventListener("contextmenu", function(e) {
			e.preventDefault();
			self.Show(e);
		});

		document.body.addEventListener("click", function(e) {
			self.Hide();
		});

		this.menuElement = document.createElement("ul");
		this.menuElement.classList = "list-group nslib-context-menu";

		this.options.forEach(option => {
			if (option.type == "separator") {
				this.menuElement.appendChild(document.createElement("hr"));
			}
			else if (option.type == "check") {
				var li = document.createElement("li");
				li.classList = "list-group-item";
				li.dataset.checked = (option.checked || true).toString();
				li.addEventListener("click", function() {
					if (this.dataset.checked == "true") {
						this.dataset.checked = "false";
						this.getElementsByClassName("bi-check-square")[0].classList = "bi-square";
					}
					else {
						this.dataset.checked = "true";
						this.getElementsByClassName("bi-square")[0].classList = "bi-check-square";
					}

					option.function(this.dataset.checked == "true");
				});

				var i = document.createElement("i");
				i.classList = "bi-check-square";
				if (option.checked == false) {
					i.classList = "bi-square";
					li.dataset.checked = "false";
				}
				
				li.appendChild(i);
				li.innerHTML += option.text;

				this.menuElement.appendChild(li);
			}
			else {
				var li = document.createElement("li");
				li.classList = "list-group-item";
				li.innerHTML = option.text;
				li.addEventListener("click", option.function);

				this.menuElement.appendChild(li);
			}
		});

		document.body.appendChild(this.menuElement);
	}

	Show(e) {
		this.menuElement.style.top = (e.clientY + ((window.pageYOffset || document.documentElement.scrollTop) - (document.documentElement.clientTop || 0))) + "px";
		this.menuElement.style.left = (e.clientX + ((window.pageXOffset || document.documentElement.scrollLeft) - (document.documentElement.clientLeft || 0))) + "px";

		if (this.menuElement != undefined)
			this.menuElement.style.display = "flex";
	}

	Hide() {
		if (this.menuElement != undefined)
			this.menuElement.style.display = "none";
	}
}
